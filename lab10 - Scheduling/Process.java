import java.util.ArrayList;
public class Process {
    protected ArrayList<Item> items;
    protected ArrayList<Item> ended_items;
    protected int status;
    protected String name;
    protected int waitTime;
    protected int lastCPUTimePC;
    /**
    *   El tiempo de arribo o el momento en que un proceso se carga al sistema
    */
    protected int ta;
    public static final int READY_STATE = 0;
    public static final int RUNNING_STATE = 1;
    public static final int BLOCKED_STATE = 2;

    public Process(String name) {
        this.name = name;
        items = new ArrayList<Item>();
        ended_items = new ArrayList<Item>();
    }

    public void addItem(Item i) {
        items.add(i);
    }

    public void addItem(String type, int time) {
        addItem(new Item(type, time));
    }

    public Item getFirstItem() {
        if(items.size() > 0) {
            return items.get(0);
        } else {
            return null;
        }
    }

    public String getName() {
        return name;
    }

    public int getWaitTime() {
        return waitTime;
    }

    public void endFirstItem(int PC) {
        Item i = items.remove(0);
        ended_items.add(i);
        waitTime += PC - lastCPUTimePC - i.getTime();
    }

    public int getTA() {
        return ta;
    }

    public void setTA(int ta) {
        this.ta = ta;
        lastCPUTimePC = ta;

    }

    @Override
    public String toString() {
        return name + ": " + items;
    }
}

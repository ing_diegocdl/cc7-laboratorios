public class Item {

    public static final String CPU = "CPU";
    public static final String IO = "IO";
    protected int time;
    protected int endTime;
    protected String type;

    public Item(String type, int time) {
        this.type = type.toUpperCase();
        this.time = time;
    }

    public int getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public void setEndTime(int PC) {
        this.endTime = PC + time;
    }

    public int getEndTime() {
        return this.endTime;
    }

    @Override
    public String toString() {
        return type + ":" + time;
    }
}

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Scheduler {
    public static final String RR = "RR";
    public static final String FIFO = "FIFO";
    protected static String politica;
    protected static BufferedReader br;
    protected static ArrayList<Process> waiting_process;
    protected static ArrayList<Process> io_waiting_process;
    protected static ArrayList<Process> process;
    protected static ArrayList<Process> ended_process;
    protected static int quantum = 0;
    protected static int PC;
    protected static Process current_process;

    public static void main(String args[]) throws Exception {
        if(args.length != 2) {
            printHelp();
            System.exit(1);
        }
        politica = args[0].toUpperCase();
        String fileName = args[1];

        process = new ArrayList<Process>();
        waiting_process = new ArrayList<Process>();
        ended_process = new ArrayList<Process>();
        io_waiting_process = new ArrayList<Process>();

        // Read file
        br = new BufferedReader(new FileReader(fileName));
        if(politica.equals(RR)){
            readQuantum();
        }
        // System.out.println(quantum);

        readProcess();
        displayState();
        execute();
    }

    public static void addProcess(Process p) {
        if(waiting_process.size() == 0) {
            waiting_process.add(p);
        } else {
            for(int i = 0; i < waiting_process.size(); i++) {
                Process p2 = waiting_process.get(i);
                if(p2.getTA() > p.getTA()) {
                    // System.out.println("proceso agregado en " + i);
                    waiting_process.add(i, p);
                    break;
                }
            }
        }
    }

    public static void addIOProcess(Process p) {
        p.getFirstItem().setEndTime(PC);
        if(io_waiting_process.size() == 0) {
            io_waiting_process.add(p);
        } else {
            for(int i = 0; i < io_waiting_process.size(); i++) {
                Process p2 = io_waiting_process.get(i);
                if(p2.getFirstItem().getEndTime() > p.getFirstItem().getEndTime()) {
                    // System.out.println("proceso agregado en " + i);
                    io_waiting_process.add(i, p);
                    break;
                }
            }
        }
    }

    public static void readQuantum() {
        try {
            String q = br.readLine();
            quantum = Integer.parseInt(q);
            br.readLine(); // solo para sacar del BR el ;
        } catch( Exception e) {
            System.err.println("Formato de quantum invalido");
            System.exit(1);
        }
    }

    public static void readProcess() throws Exception {
        String line;
        while((line = br.readLine()) != null) {
            System.out.println(line);
            Process p = new Process(line);
            while(!(line = br.readLine()).trim().equals(";")) {
                System.out.println("\t" + line);
                String l[] = line.split(":");
                if(!l[0].equals("TA")) {
                    p.addItem(l[0], Integer.parseInt(l[1]));
                } else {
                    p.setTA(Integer.parseInt(l[1]));
                }
            }
            addProcess(p);
            System.out.println("---------------");
        }
    }

    public static void execute() {
        executeFIFO();
    }

    public static void executeFIFO() {
        PC = 0;
        current_process = null;
        int pc_0 = 0;
        while(true) {
            if(waiting_process.size() > 0 ) {
                // revisamos si ya se cumplio el tiempo de arribo
                if(waiting_process.get(0).getTA() <= PC) {
                    // remover el proceso de la lista de espera y lo pasamos a la
                    // lista para ser ejecutado
                    process.add(waiting_process.remove(0));
                }
            }

            if(io_waiting_process.size() > 0) {
                if(io_waiting_process.get(0).getFirstItem().getEndTime() <= PC) {
                    Process p = io_waiting_process.remove(0);
                    p.endFirstItem(PC);
                    if(p.getFirstItem() != null)
                        process.add(p);
                    else
                        ended_process.add(p);
                }
            }
            // si hay current_process entonces se revisa si ya cumplio el tiempo de ejecucion
            if( current_process != null && current_process.getFirstItem().getTime() == (PC - pc_0)) {
                current_process.endFirstItem(PC);
                pc_0 = PC;
                Item i = current_process.getFirstItem();
                if(i == null) {
                    ended_process.add(current_process);
                    current_process = null;

                } else if ( i.getType().equals(Item.IO)) {
                    addIOProcess(current_process);
                    current_process = null;
                }
            }
            // revisamos si hay no hay procesos ejecutandose
            if(current_process == null) {
                System.out.println(waiting_process);
                System.out.println(io_waiting_process);
                if(process.size() > 0) {
                    current_process = process.remove(0);
                    pc_0 = PC;
                }
            }
            displayState();
            if(current_process == null && waiting_process.size() == 0 && io_waiting_process.size() == 0) {
                break;
            }
            PC++;
            try { Thread.sleep(250); } catch(Exception e){}
        }
        // displayStats();
    }

    public static void displayState() {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("Procesando : " + current_process);
        System.out.println("PC: " + PC + " ticks");
        System.out.println("Procesos en cola: ");
        for (int i = 0; i < process.size() ; i++ ) {
            System.out.println(process.get(i));
        }
    }

    public static void displayStats() {
        for(Process p: ended_process)
            System.out.println(p.getName() + " " + p.getWaitTime() );
    }

    public static void printHelp() {
        System.out.println("Laboratorio 10 - CC7 - Diego Calderon");
        System.out.println("Simulador de Procesador para FIFO y para RR");
        System.out.println("uso:");
        System.out.println("\tjava Scheduler [fifo|rr] file.txt");
    }
}

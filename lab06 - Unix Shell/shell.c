#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>

#define BUFFER_SIZE 50
#define MAX_LINE 80 /* 80 chars per line, per command, should be enough. */

static char buffer[BUFFER_SIZE];

char inputBuffer[MAX_LINE]; /* buffer to hold the command entered */
int background;
/* equals 1 if a command is followed by '&' */
char *args[MAX_LINE/+1];/* command line (of 80) has	max of 40 arguments */


typedef struct node {
	char *command;
	struct node *next;
} node;

node* headHistory = NULL;
int listSize = 0;

void parse(char inputBuffer[], char *args[], int* background) {
	char *delimiter = " ";
		
		// reads the command
		int i = 0;
		int len = strlen(inputBuffer);
		// inputBuffer[len-1] = '\0';
		char *token = strtok(inputBuffer, delimiter);

		while(token != NULL) {
			args[i] = token;
			token = strtok(NULL, delimiter);
			i++;
		}

		if( i > 1 && *args[i-1] ==  '&') {
			args[i-1] = NULL;
			*background = 0;
		} else {
			*background = 1;
		}
		args[i] = NULL;
}

void addHistory(char *cmd) {
	int aux = 0;
	node* nodeTemp;
	node* newNode = (node*)malloc(sizeof(node));
	char* cmd_copy = (char*)malloc(strlen(cmd)*sizeof(char) + 1);
	strcpy(cmd_copy, cmd);
	newNode->command = cmd_copy;
	listSize++;
	if(headHistory == NULL) {
		newNode->next = NULL;
		headHistory = newNode;
	} else {
		// printf("%s\n", "nuevo nodo agregado" );
		newNode->next = headHistory;
		headHistory = newNode;
		nodeTemp = headHistory;
		if(listSize > 10) {
			while(aux < 9){
				nodeTemp = nodeTemp->next; 
				aux++;	
			} 
			// free(nodeTemp->next->command);
			// free(nodeTemp->next);

			nodeTemp->next = NULL;
		}
		printf("%s\n", "eliminado" );
	}
}


void printHistory(){
	node* list = headHistory;
	int i = 0;
	while(list != NULL) {
		printf("%d %s\n", i, list->command);
		i++;
		list = list->next;
	}
}

/**
 * setup() reads in the next command line, separating it into distinct tokens
 * using whitespace as delimiters. setup() sets the args parameter as a
 * null-terminated string.
*/
void setup(char inputBuffer[], char *args[],int *background) {
	int length; /* # of characters in the command line */
	//read what the user enters on the command line
	length = read(STDIN_FILENO, inputBuffer, MAX_LINE);
	inputBuffer[length-1] = '\0';
	// printf("cmd: \"%s\"\n", inputBuffer );
	// printf("Length %d\n", strlen(inputBuffer) );
	if(strlen(inputBuffer) > 1) {
		addHistory(inputBuffer);
		parse(inputBuffer, args, background);
	}
}

void execute(char **cmd, int *background) {
	int pid = -1;
	pid = fork();
	int status;
	if (pid >= 0) {
		if(pid == 0) {
			// (2) the child process will invoke execvp()
			if(execvp(*cmd, cmd) < 0) {
				printf("%s\n", "Ocurrio un error al momento de ejecutar el comando");
				exit(1);
			}
		} else {
			if(*background) {
				while (wait(&status) != pid)  ;
			}
			
		}
	} else {
		printf("%s\n", "Ocurrio un error y no pudo ejecutarse el comando" );
	}
}

/* the signal handler function */
void handle_SIGINT() {
	printHistory();
	// exit(0);
}

void reExecute(char c, char **args, int *background) {
	node *temp = headHistory;
	while(temp != NULL) {
		if(temp->command[0] == c) {
			printf("%s\n", temp->command);
			parse(temp->command, args, background);
			execute(args, background);
			break;
		}
		temp = temp->next;
	}
}

int main(void) {
	/* set up the signal handler */
	struct sigaction handler;
	handler.sa_handler = handle_SIGINT;
	sigaction(SIGINT, &handler, NULL);
	strcpy(buffer,"Caught <ctrl><c>\n");

	// while(1);
	while (1){
		/* Program terminates normally inside setup */
		background = 0;
		printf("COMMAND->\n");
		inputBuffer[0] = '\0';
		setup(inputBuffer,args,&background); /* get next command */
		//  the steps are:
		// 	(1) fork a child process using fork()
		// 	(2) the child process will invoke execvp()
		// 	(3) if background == 1, the parent will wait, otherwise returns to the setup() function. 
		
		// // (1) fork a child process using fork()
		if(strlen(inputBuffer) > 0){
			// printf("%c\n", args[0][0] );
			if(args[0][0] == 'r') {
				printf("%s %s\n", "reejecutar funcion que empieza en ", args[1]);
				reExecute(args[1][0], args, &background);
			} else {
				execute(args, &background);
			}
		}
		
	}
	return 0;
}
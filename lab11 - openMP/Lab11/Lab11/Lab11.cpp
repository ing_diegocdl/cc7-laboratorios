// Lab11.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <cassert>
#include <cstdio>
#include <string>
#include <algorithm>
#include <vector>
#include <malloc.h>
volatile DWORD dwStart;
volatile int global = 0;
volatile int secciones = 2;

///////////////////////////////////////////////////////////////////////////////
//
// Constants
// Increase to measure average performance; larger values mean
// longer test runs.
const int gTestRuns = 100;

// Qsort constants
const int gQsortNum = 1000000;
const int gMaxStrChars = 16;
// General-purpose constants
const LARGE_INTEGER gZero = { 0, 0 };
const __int64 gNanosecondsPerSecond = 1000000000; // 10^9


///////////////////////////////////////////////////////////////////////////////
//
// Simple timer class
class Timer
{
public:
	Timer()
		: mStart( gZero )
	{
		Restart();
	}

	void Restart()
	{
		QueryPerformanceCounter( &mStart );
	}

	__int64 GetElapsed() const
	{
		LARGE_INTEGER end;
		QueryPerformanceCounter( &end );
		return end.QuadPart - mStart.QuadPart;
	}

	private:
		LARGE_INTEGER mStart;
};

///////////////////////////////////////////////////////////////////////////////
//
// Partition
//
// Standard quicksort algorithm for partitioning an array. Patterned on
// Sedgewick, Algorithms in C++.
template< typename T >
int Partition( T* arr, int lo, int hi )
{
	int i = lo - 1;
	int j = hi;
	T v = arr[ hi ];
	for( ;; )
	{
		while( arr[ ++i ] < v )
			;
		while( v < arr[ --j ] )
		{
			if( j == lo )
				break;
		}
		if( i >= j )
			break;
		std::swap( arr[i], arr[j] );

	}
	std::swap( arr[i], arr[hi] );
	return i;
}

///////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
//
// SerialQsort
template< typename T >
void SerialQsort( T* arr, int lo, int hi )
{
	if( hi <= lo )
		return;
	
	int n = Partition( arr, lo, hi );
	SerialQsort( arr, lo, n-1 );
	SerialQsort( arr, n+1, hi );
}

template< typename T >
void ParallelQsort( T* arr, int lo, int hi )
{
	//PREGUNTA 4
	//PARALLELIZE ME!!!
	if( hi <= lo )
		return;

	if(secciones == 2) {
		// 2 secciones
		int n = Partition( arr, lo, hi );
		#pragma omp parallel sections
		{
			#pragma omp section
			SerialQsort( arr, lo, n-1 );

			#pragma omp section
			SerialQsort( arr, n+1, hi );
		}
	} else if(secciones == 4) {
		// -------------------------
		// 4 secciones
		// -------------------------
		// mitad del arreglo
		int n2 = Partition( arr, lo, hi );

		// mitad de la primera mitad
		int n1 = Partition( arr, lo, n2);

		// mitad de la segunda mitads
		int n3 = Partition( arr, n2, hi );

		#pragma omp parallel sections
		{
			#pragma omp section
			SerialQsort( arr, lo, n1-1 );

			#pragma omp section
			SerialQsort( arr, n1+1, n2-1);

			#pragma omp section
			SerialQsort( arr, n2+1, n3-1);

			#pragma omp section
			SerialQsort( arr, n3+1, hi );
		}
	} else if(secciones == 8) 
	{
		// 8 secciones
		// |     |     |     |     ||     |     |     |     | = arr
		// lo    n1    n2     n3   n4     n5   n6    n7    hi
		

		// 8 secciones
		// parte a la mitad mitad a arr
		int n4 = Partition( arr, lo, hi );

		// primera mitad de arr
		int n2 = Partition( arr, lo, n4-1);
		int n1 = Partition( arr, lo, n2-1);
		int n3 = Partition( arr, n2+1, n4-1);

		// segunda mitad de arr
		int n6 = Partition( arr, n4+1, hi);
		int n5 = Partition( arr, n4+1, n6-1);
		int n7 = Partition( arr, n6+1, hi);


		#pragma omp parallel sections
		{
			#pragma omp section
			SerialQsort( arr, lo, n1-1 );

			#pragma omp section
			SerialQsort( arr, n1+1, n2-1);

			#pragma omp section
			SerialQsort( arr, n2+1, n3-1);

			#pragma omp section
			SerialQsort( arr, n3+1, n4-1 );

			#pragma omp section
			SerialQsort( arr, n5+1, n6-1 );

			#pragma omp section
			SerialQsort( arr, n7+1, hi);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////
//
// Qsort performance measurements
template< typename T, typename Generator >
void Qsort( Generator g )
{
	std::vector< T > arr( gQsortNum );
	dwStart = GetTickCount();
	__int64 serialTicks = 0;
	for( int i = 0; i < gTestRuns; ++i )
	{
		srand( i );
		generate( arr.begin(), arr.end(), g );
		Timer serialTimer;
		SerialQsort( &arr[0], 0, gQsortNum-1 );
		serialTicks += serialTimer.GetElapsed();
	}

	printf( "Serial version : %I64d ticks\n", serialTicks );
	printf_s("Serial Version TEST: %d milliseconds\n", GetTickCount() - dwStart);

	// Save a copy for verification
	std::vector< T > verify( arr );
	dwStart = GetTickCount();
	__int64 parallelTicks = 0;
	for( int i = 0; i < gTestRuns; ++i )
	{
		srand( i );
		generate( arr.begin(), arr.end(), g );
		Timer parallelTimer;
		ParallelQsort( &arr[0], 0, gQsortNum-1 );
		parallelTicks += parallelTimer.GetElapsed();
	}

	printf( "Parallel version: %I64d ticks\n", parallelTicks );
	printf_s("Parallel Version TEST: %d milliseconds\n", GetTickCount() - dwStart);

	// Verify
	for( int i = 0; i < gQsortNum; ++i )
		assert( arr[i] == verify[i] );
}

double test2(int num_steps) {
	int i;
	global++;
	double x, pi, sum = 0.0, step;

	step = 1.0 / (double) num_steps;
	
	//PREGUNTA 6
	#pragma omp parallel for reduction(+:sum) private(x) num_threads(8)
	for (i = 1; i <= num_steps; i++) {
		x = (i - 0.5) * step;
		sum = sum + 4.0 / (1.0 + x*x);
	}

	pi = step * sum;
	return pi;
}

double test(int num_steps) {
	int i;
	global++;
	double x, pi, sum = 0.0, step;
	step = 1.0 / (double) num_steps;
	for (i = 1; i <= num_steps; i++) {
		x = (i - 0.5) * step;
		sum = sum + 4.0 / (1.0 + x*x);
	}

	pi = step * sum;
	return pi;
}

int main(int argc, char* argv[]) {
	double d;
	int n = 1000000000;
	if (argc > 1)
		n = atoi(argv[1]);

	//PREGUNTA 2
	#pragma omp parallel num_threads(4)
	{
		int i = omp_get_thread_num();
		printf_s("Hello from thread %d\n", i);
	}

	//PREGUNTA 3
	omp_set_num_threads(8);

	// Max threads available to OpenMP
	printf( "Maximum OpenMP threads: %d\n\n", omp_get_max_threads() );
	printf( "Qsort (int) 2 secciones\n" );
	secciones = 2;
	Qsort< int >( rand );

	printf( "\nQsort (int) 4 secciones\n" );
	secciones = 4;
	Qsort< int >( rand );

	printf( "\nQsort (int) 8 secciones\n" );
	secciones = 8;
	Qsort< int >( rand );


	//PREGUNTA 5
	dwStart = GetTickCount();
	d = test(n);
	printf_s("For %d steps in TEST, pi = %.15f, %d milliseconds\n", n, d, GetTickCount() - dwStart);
	dwStart = GetTickCount();
	d = test2(n);
	printf_s("For %d steps, pi = %.15f, %d milliseconds\n", n, d, GetTickCount() - dwStart);

	dwStart = GetTickCount();
	d = test2(n);
	printf_s("For %d steps, pi = %.15f, %d milliseconds\n", n, d, GetTickCount() - dwStart);
}
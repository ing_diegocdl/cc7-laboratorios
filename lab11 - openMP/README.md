# Lab11 - openMP

1. ¿Qué es OpenMP?
    - OpenMP es un API para C/C++ que permite la implementacion de programas que aprovechen `parallel programming`. Esta es soportada por multiples compiladores como el de visual studio o gcc y es utilizada ampliamente en el desarrollo de videojuegos por las caracteristicas de los codigos de los juegos que pueden ser paralelizados en varias tareas. Una de las principales ventajas de OpenMP es que no es necesario agregar muchas lineas de codigo para poder paralelizar secciones de codigo y si la arquitectura para la que se esta compilando o si el compilador no soporta OpenMP simplemente lo compila y funciona normal.

2. Analizando los resultados, ¿Cuál es la salida de la siguiente sección de código?
    ```
    #pragma omp parallel num_threads(4)
    {
        int i = omp_get_thread_num();
        printf_s("Hello from thread %d\n", i);
    }
    ```
    La salida seria la siguiente:
    ```
    Hello from thread 0
    Hello from thread 1
    Hello from thread 2
    Hello from thread 3
    ```
    cada thread imprime un `Hello from thread #`

3. ¿Qué hace la instrucción `omp_set_num_threads(2);`?
    - Establece la cantidad de threads que utilizara openMP por defecto en 2 y utilizara este número de threads a menos que se le indique un numero diferente en cada `parallel section`

4. Paralelice la función ParallelQSort. Indique los resultados y mejoras en performance obtenidas utilizando:
    2 Threads:
    - a) 2 secciones
        - Serial version : 15563208 ticks
        - Serial Version TEST: 7860 milliseconds
        - Parallel version: 12200793 ticks
        - Parallel Version TEST: 6468 milliseconds
    - b) 4 secciones
        - Serial version : 15436764 ticks
        - Serial Version TEST: 7797 milliseconds
        - Parallel version: 11422112 ticks
        - Parallel Version TEST: 6094 milliseconds
    - c) 8 secciones
        -

        ## Maximum OpenMP threads: 2

        Qsort (int) 2 secciones
        Serial version : 15469847 ticks
        Serial Version TEST: 7813 milliseconds
        Parallel version: 12070788 ticks
        Parallel Version TEST: 6391 milliseconds

        Qsort (int) 4 secciones
        Serial version : 15520818 ticks
        Serial Version TEST: 7844 milliseconds
        Parallel version: 11263045 ticks
        Parallel Version TEST: 6031 milliseconds

        Qsort (int) 8 secciones
        Serial version : 15353451 ticks
        Serial Version TEST: 7750 milliseconds
        Parallel version: 8255753 ticks
        Parallel Version TEST: 4734 milliseconds
        For 1000000000 steps in TEST, pi = 3.141592653589971, 4406 milliseconds
        For 1000000000 steps, pi = 3.141592653589769, 1141 milliseconds

        ## Maximum OpenMP threads: 4

        Qsort (int) 2 secciones
        Serial version : 15331008 ticks
        Serial Version TEST: 7750 milliseconds
        Parallel version: 14014297 ticks
        Parallel Version TEST: 7500 milliseconds

        Qsort (int) 4 secciones
        Serial version : 16407405 ticks
        Serial Version TEST: 8297 milliseconds
        Parallel version: 12366800 ticks
        Parallel Version TEST: 6766 milliseconds

        Qsort (int) 8 secciones
        Serial version : 15059252 ticks
        Serial Version TEST: 7609 milliseconds
        Parallel version: 8546442 ticks
        Parallel Version TEST: 5110 milliseconds
        For 1000000000 steps in TEST, pi = 3.141592653589971, 4422 milliseconds
        For 1000000000 steps, pi = 3.141592653589769, 1140 milliseconds

        ## Maximum OpenMP threads: 8

        Qsort (int) 2 secciones
        Serial version : 15407864 ticks
        Serial Version TEST: 7797 milliseconds
        Parallel version: 16445851 ticks
        Parallel Version TEST: 8922 milliseconds

        Qsort (int) 4 secciones
        Serial version : 15890286 ticks
        Serial Version TEST: 8046 milliseconds
        Parallel version: 14723079 ticks
        Parallel Version TEST: 8188 milliseconds

        Qsort (int) 8 secciones
        Serial version : 16125754 ticks
        Serial Version TEST: 8156 milliseconds
        Parallel version: 9978621 ticks
        Parallel Version TEST: 6187 milliseconds
        For 1000000000 steps in TEST, pi = 3.141592653589971, 4547 milliseconds
        For 1000000000 steps, pi = 3.141592653589769, 1094 milliseconds


    - *Probado con: Windows 8.1, 12Gb RAM, Core i7 4700MQ, 4 cores, 8 Threads.*

5. ¿Cuáles son las diferencias en performance entre llamar a la función test y la función test2?

6. ¿Qué hace la instrucción `#pragma omp parallel for reduction(+:sum) private(x) num_threads(8)`?
    - Especifica que se desea que el for se ejecute paralelamente con 8 threds la parte de la reduccion sirve para que cada thread tenga una copia privada de la variable sum y cuando finalicen se sumen todos los resultados de sum en la variable global, esto permite un mejor rendimiento reduciendo los data races.

7. ¿Qué sucede si cambia el parámetro a `num_threads` en la línea `#pragma omp parallel for reduction(+:sum) private(x) num_threads(8)`?
    - El for se ejecuta con la cantidad de threads que se le especifique en num_threads y esto afectara el performance del programa, no siempre una mayor cantidad de threads proporcione una mejora significativa al performance inclusive puede existir ocasiones en donde con demasiados threads se obtengan resultados opuestos a los deseados.

8. ¿Qué sucede si agrega otra llamada a test2? Es decir en el código tendría dos veces el bloque :
    ```
    dwStart = GetTickCount();
    d = test2(n);
    printf_s("For %d steps, pi = %.15f, %d milliseconds\n", n, d, GetTickCount() - dwStart);
    ```
    ¿Los resultados se mantienen o existe alguna diferencia en los tiempos de ejecución en las llamadas a test2? ¿Por qué?

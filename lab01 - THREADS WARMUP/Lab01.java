/**
*	Ciencias de la Computacion 7
*	Laboratorio 1 - Threads Warmup
*	@author Diego Calderónc
*/
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Lab01 {
	public static void main(String[] args) {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Ingrese el numero N: ");
		try {
			int n = Integer.parseInt(br.readLine());
			Thread t1 = new Thread(new NumerosPrimosThread(n));
			Thread t2 = new Thread(new NumerosPrimosThread(n));
			t1.start();
			t2.start();
		} catch(Exception e) {
			
		}
	}
}

class NumerosPrimosThread implements Runnable {
	protected static int contador;
	protected int id;
	protected int n;

	public NumerosPrimosThread(int n) {
		id = ++contador;
		this.n = n;
	}

	public void run() {
		long found = 0;
		for (long i = 0; found < n; i++ ) {
			long suma = 0;
			for(long j = 1; j < i; j++) {
				if(i%j == 0) {
					suma += j;
				}
			}
			if(suma == i) {
				System.out.println("Thread " + id + ": El numero "  + i + " es perfecto");
				++found;
			} else {
				System.out.println("Thread " + id + ": El numero "  + i + " no es perfecto");
			}
			System.out.flush();
		}
	}
}

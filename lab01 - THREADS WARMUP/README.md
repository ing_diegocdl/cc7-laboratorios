# Lab01 - THREADS WARMUP

##Descripción

Debe desarrollar una aplicación que sea capaz de imprimir a pantalla una cantidad N de números perfectos. Debe levantar 2 threads y cada uno de ellos recibe como parámetro el número N ingresado por el usuario. Cada thread procederá a buscar los primeros N números perfectos, comenzando desde el numero 0.Si, los dos threads hacen exactamente lo mismo!. El objetivo de este laboratorio es recordar la programación con threads en java y analizar la ejecución de dos tareas idénticas que se encuentran ejecutando concurrentemente.
Cada thread deberá imprimir a pantalla lo siguiente:

```
	Thread <id> : El numero <i> es o no es perfecto.
	<id>: Un numero que identifica de manera única a cada thread. 
	<i>: El numero que se está evaluando en un momento determinado.
```

**Ejemplo:**

```
	Thread 1 : El numero 1 es perfecto
	Thread 1 : El numero 2 no perfecto
```

##Preguntas

- **¿Qué es un thread?**
- **¿Cuál es la diferencia entre implementar un Thread como un extends de la clase Thread y con un Runnable?**
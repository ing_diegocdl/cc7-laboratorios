# Lab05: Creacion de Procesos
## Windows
**Nota**: para compilar y ejecutar el programa (`ProccessCreatorWin.cpp`), descargar Dev-C portable que se encuentra en la sección de descargas del repositorio.

### Preguntas
1. **¿Cuál es el resultado de este programa?**
2. **Ejecute el programa `procexp.exe` que se encuentra en la carpeta Soporte a los Laboratorios del GES. Tome su tiempo para revisar y explorar las funcionalidades de la aplicación.**
3. **Con el `procexp.exe` corriendo, vuelva a ejecutar el código anterior. Analice y revise la estructura del árbol de procesos. ¿Qué relación tiene su programa con el `mspaint.exe`? **
4. **¿Qué sucede en el árbol de procesos si cierra la ventana de consola asociada a su programa? ¿Mspaint.exe continua activo?**
5. **Para analizar el funcionamiento de la instrucción WaitForSingleObject(), pruebe comentar la llamada a dicha función y observe el comportamiento de su programa en el árbol de procesos.**
6. **¿Existe alguna similitud entre las instrucciones wait() y WaitForSingleObject() de los programas que ejecutó en Linux y Windows?**

### Respuestas
1. *El programa abre Paint como un proceso hijo y espera a que se cierre Paint para terminar su ejecución*
2. *-*
3. *El proceso del programa `mspaint.exe` aparece como un hijo del proceso correspondiente a nuestro programa*
4. *Al cerrar la ventana de consola el proceso correspondiente se cierra y luego `mspaint.exe` cambia en el arbol y ya no pertenece como hijo de algun otro proceso*
5. *Al abrir el programa ya no nos aparece la ventana de consola y abre `mspaint.exe` el cual en el arbol de procesos no pertenece a otro proceso.*

## Linux
1. **¿Cuál es el resultado de este programa?**
	- Muestra el output del comando ls del directorio donde se ejecuto el programa, ademas muestra el pid del proceso padre y del proceso hijo.
2. **Describa paso a paso lo que sucede en este programa.**
	- Se crea un child process con `fork()` y se verifica que el pid que retorno la funcion no sea menor a 0 dado que entonces existiria un error y se termina el programa, luego tiene una verificacion con un if para saber que proceso es el que esta ejecutando el codigo y asi asignar la parte correspondiente a cada proceso, para el proceso hijo se ejecuta el comando ls por medio de la funcion execlp(), luego si no es el proceso hijo despliega otros mensajes y espera a que el proceso hijo termine y luego termina el programa.
3. **¿Qué sucede si comenta la instrucción `wait(NULL)`?, ¿Cambian los resultados? ¿Por qué?**
	- Cambia el orden del output del programa, se despliega toda la informacion del proceso padre primero y luego saca el output del comando ls, esto es por que la instruccion `wait(NULL)` hace que el proceso padre espere a que finalice el hijo para continuar.
4. **¿Cuál es la función de los if que validan la variable pid (process id)?**
	- Que no todos los procesos ejecuten todo el codigo, por lo que los if verifican el pid para saber que codigo corresponde a cada proceso.
5. **¿Qué sección de código o programa se ejecuta cuando se crea el nuevo proceso? (Indique lo que sucede al momento de ejecutar las instrucciones `fork()` y `execlp()` )**
	- la instruccion `fork()` crea un proceso y devuelve el pid, la instruccion `execlp()` ejecuta un archivo como un nuevo proceso con los parametros que se le especifican en la llamada de la funcion.

## General
1. **Discuta sobre las similitudes y diferencias entre la creación de procesos en Linux y en Windows.**
	- En ambos sistemas operativos se pueden crear sub procesos 
	- En ambos sistemas operativos se puede esperar o no a que el sub proceso termine de ejecutarse.
	- La creacion de procesos es diferente entre Linux y Windows, en Linux comparte el mismo codigo con el padre por lo que debemos agregar if's para diferenciar que proceso ejecuta que seccion del codigo, en cambio con windows son un poco mas independientes.
public class Runner {

	Thread threadSumador;
	Thread threadRestador;
	Comunicador comunicador;

	public Runner() {
		comunicador = new Comunicador();
		threadSumador = new Thread(new Sumador(comunicador));
		threadRestador = new Thread(new Restador(comunicador));
	}

	public void startThreads() throws Exception {
		threadSumador.start();
		threadRestador.start();
		threadSumador.join();
		threadRestador.join();
	}

	public static void main(String[] args) throws Exception {
		Runner r = new Runner();
		r.startThreads();
	}
}

public class Comunicador {
	protected int x;

	public Comunicador(){
		x = 0;
	}

	/**
	*	Decrementa el contador si es mayor a 0 de lo contario espera a que el valor
	* cumpla la condicion de ser mayor de 0
	*	@return el valor del contador
	*/
	public synchronized int resta() {
		if(x == 0) {
			try {
				wait();
			} catch(Exception e) {}
		}
		x--;
		return x;
	}
	/**
	* Incrementa en uno el contador y notifica a los threads que esperen que sea
	* mayor que 0 el valor
	*	@return el valor del contador
	*/
	public synchronized int suma() {
		x++;
		notify();
		return x;
	}

}

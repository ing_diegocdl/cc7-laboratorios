import java.util.Random;
public class Restador implements Runnable {
	protected Comunicador comunicador;

	public Restador(Comunicador c) {
		comunicador = c;
	}

	@Override
	public void run() {
		Random r = new Random();
		for (int i = 0; i < 100 ; i++) {
			int x = comunicador.resta();
			System.out.println("Thread Resta: " + x);
			try {
				Thread.sleep(r.nextInt(5) + 1); // espera tiempo random entre 1 y 5 seg
			} catch(Exception e) {}
		}
	}

}

import java.util.Random;
public class Sumador implements Runnable {
	Comunicador comunicador;

	public Sumador(Comunicador c) {
		comunicador = c;
	}

	@Override
	public void run() {
		Random r = new Random();
		for (int i = 0; i < 100 ; i++ ) {
			int x = comunicador.suma();
			System.out.println("Thread Sumador : " + x);
			try {
				Thread.sleep(r.nextInt(5) + 1); // espera tiempo random entre 1 y 5 seg
			} catch(Exception e) {}
		}
	}
}

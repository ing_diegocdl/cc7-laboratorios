
obj/boot/boot.out:     file format elf32-i386


Disassembly of section .text:

00007c00 <start>:
start:
  # FIRST PHASE: Register y operation mode setup.
  # Assemble for 16-bit mode
  
	.code16  
        cli
    7c00:	fa                   	cli    

	# Set up the important data segment registers (DS, ES, SS).
	xorw	%ax,%ax			# Segment number zero
    7c01:	31 c0                	xor    %eax,%eax
	movw	%ax,%ds			# -> Data Segment
    7c03:	8e d8                	mov    %eax,%ds
	movw	%ax,%es			# -> Extra Segment
    7c05:	8e c0                	mov    %eax,%es
	movw	%ax,%ss			# -> Stack Segment
    7c07:	8e d0                	mov    %eax,%ss



	lgdt	gdtdesc		# load GDT: mandatory in protected mode
    7c09:	0f 01 16             	lgdtl  (%esi)
    7c0c:	14 7d                	adc    $0x7d,%al
	movl	%cr0, %eax	# Turn on protected mode
    7c0e:	0f 20 c0             	mov    %cr0,%eax
	orl	$CR0_PE_ON, %eax
    7c11:	66 83 c8 01          	or     $0x1,%ax
	movl	%eax, %cr0
    7c15:	0f 22 c0             	mov    %eax,%cr0

    # CPU magic: jump to relocation, flush prefetch queue, and
	# reload %cs.  Has the effect of just jmp to the next
	# instruction, but simultaneously loads CS with
	# $PROT_MODE_CSEG.
	DATA32 ljmp	$PROT_MODE_CSEG, $protcseg
    7c18:	66 ea 20 7c 00 00    	ljmpw  $0x0,$0x7c20
    7c1e:	08 00                	or     %al,(%eax)

00007c20 <protcseg>:
	# to generate code for that mode
	.code32
protcseg:	

	# Set up the protected-mode data segment registers
	movw	$PROT_MODE_DSEG, %ax	# Our data segment selector
    7c20:	66 b8 10 00          	mov    $0x10,%ax
	movw	%ax, %ds		# -> DS: Data Segment
    7c24:	8e d8                	mov    %eax,%ds
	movw	%ax, %es		# -> ES: Extra Segment
    7c26:	8e c0                	mov    %eax,%es
	movw	%ax, %fs		# -> FS
    7c28:	8e e0                	mov    %eax,%fs
	movw	%ax, %gs		# -> GS
    7c2a:	8e e8                	mov    %eax,%gs
	movw	%ax, %ss		# -> SS: Stack Segment
    7c2c:	8e d0                	mov    %eax,%ss
	#de las actividades asignadas
	#
	#

	# D = 44
	movb   $0x44,0xB8000 
    7c2e:	c6 05 00 80 0b 00 44 	movb   $0x44,0xb8000
	movb   $0x1E,0xB8001
    7c35:	c6 05 01 80 0b 00 1e 	movb   $0x1e,0xb8001

	# I = 49
	movb   $0x49,0xB8002 
    7c3c:	c6 05 02 80 0b 00 49 	movb   $0x49,0xb8002
	movb   $0x1E,0xB8003
    7c43:	c6 05 03 80 0b 00 1e 	movb   $0x1e,0xb8003

	# E = 45
	movb   $0x45,0xB8004
    7c4a:	c6 05 04 80 0b 00 45 	movb   $0x45,0xb8004
	movb   $0x1E,0xB8005
    7c51:	c6 05 05 80 0b 00 1e 	movb   $0x1e,0xb8005

	# G = 47
	movb   $0x47,0xB8006 
    7c58:	c6 05 06 80 0b 00 47 	movb   $0x47,0xb8006
	movb   $0x1E,0xB8007
    7c5f:	c6 05 07 80 0b 00 1e 	movb   $0x1e,0xb8007

	# O = 4F
	movb   $0x4F,0xB8008
    7c66:	c6 05 08 80 0b 00 4f 	movb   $0x4f,0xb8008
	movb   $0x1E,0xB8009
    7c6d:	c6 05 09 80 0b 00 1e 	movb   $0x1e,0xb8009


	# ' ' = 20
	movb   $0x20,0xB800A
    7c74:	c6 05 0a 80 0b 00 20 	movb   $0x20,0xb800a
	movb   $0x1E,0xB800B
    7c7b:	c6 05 0b 80 0b 00 1e 	movb   $0x1e,0xb800b
# 45
# 52
# 4F
# 4E
	# C = 43
	movb   $0x43,0xB800C
    7c82:	c6 05 0c 80 0b 00 43 	movb   $0x43,0xb800c
	movb   $0x1E,0xB800D
    7c89:	c6 05 0d 80 0b 00 1e 	movb   $0x1e,0xb800d

	# A = 41
	movb   $0x41,0xB800E
    7c90:	c6 05 0e 80 0b 00 41 	movb   $0x41,0xb800e
	movb   $0x1E,0xB800F
    7c97:	c6 05 0f 80 0b 00 1e 	movb   $0x1e,0xb800f

	# L = 4C
	movb   $0x4C,0xB8010
    7c9e:	c6 05 10 80 0b 00 4c 	movb   $0x4c,0xb8010
	movb   $0x1E,0xB8011
    7ca5:	c6 05 11 80 0b 00 1e 	movb   $0x1e,0xb8011

	# D = 44
	movb   $0x44,0xB8012
    7cac:	c6 05 12 80 0b 00 44 	movb   $0x44,0xb8012
	movb   $0x1E,0xB8013	
    7cb3:	c6 05 13 80 0b 00 1e 	movb   $0x1e,0xb8013

	# E = 45
	movb   $0x45,0xB8014
    7cba:	c6 05 14 80 0b 00 45 	movb   $0x45,0xb8014
	movb   $0x1E,0xB8015
    7cc1:	c6 05 15 80 0b 00 1e 	movb   $0x1e,0xb8015

	# R = 52
	movb   $0x52,0xB8016
    7cc8:	c6 05 16 80 0b 00 52 	movb   $0x52,0xb8016
	movb   $0x1E,0xB8017
    7ccf:	c6 05 17 80 0b 00 1e 	movb   $0x1e,0xb8017

	# O = 4F
	movb   $0x4F,0xB8018
    7cd6:	c6 05 18 80 0b 00 4f 	movb   $0x4f,0xb8018
	movb   $0x1E,0xB8019
    7cdd:	c6 05 19 80 0b 00 1e 	movb   $0x1e,0xb8019

	# N = 4E
	movb   $0x4E,0xB801A
    7ce4:	c6 05 1a 80 0b 00 4e 	movb   $0x4e,0xb801a
	movb   $0x1E,0xB801B
    7ceb:	c6 05 1b 80 0b 00 1e 	movb   $0x1e,0xb801b

	call bootmain
    7cf2:	e8 5f 00 00 00       	call   7d56 <bootmain>

00007cf7 <hang>:
	
hang:
	jmp hang
    7cf7:	eb fe                	jmp    7cf7 <hang>
    7cf9:	8d 76 00             	lea    0x0(%esi),%esi

00007cfc <gdt>:
	...

00007d04 <gdt_code>:
    7d04:	ff                   	(bad)  
    7d05:	ff 00                	incl   (%eax)
    7d07:	00 00                	add    %al,(%eax)
    7d09:	9a cf 00 ff ff 00 00 	lcall  $0x0,$0xffff00cf

00007d0c <gdt_data>:
    7d0c:	ff                   	(bad)  
    7d0d:	ff 00                	incl   (%eax)
    7d0f:	00 00                	add    %al,(%eax)
    7d11:	92                   	xchg   %eax,%edx
    7d12:	cf                   	iret   
	...

00007d14 <gdt_end>:
    7d14:	17                   	pop    %ss
    7d15:	00 fc                	add    %bh,%ah
    7d17:	7c 00                	jl     7d19 <gdt_end+0x5>
	...

00007d1a <cls>:
#define BACKGROUND 0x1E

const char *cadena = "Bienvenido al GOS";

void cls(unsigned int offset)
{
    7d1a:	55                   	push   %ebp
    7d1b:	89 e5                	mov    %esp,%ebp
    7d1d:	8b 4d 08             	mov    0x8(%ebp),%ecx
	unsigned char *vidmem = (unsigned char *)0xB8000;
	const long size = 80*25;
  vidmem += offset*2;
    7d20:	8d 94 09 00 80 0b 00 	lea    0xb8000(%ecx,%ecx,1),%edx
	long loop;

   	for (loop=offset; loop<size; loop++) 
    7d27:	89 c8                	mov    %ecx,%eax
    7d29:	3d cf 07 00 00       	cmp    $0x7cf,%eax
    7d2e:	7f 11                	jg     7d41 <cls+0x27>
   	{
      if (loop >= offset){
    7d30:	39 c8                	cmp    %ecx,%eax
    7d32:	72 0a                	jb     7d3e <cls+0x24>
          *vidmem++ = 0;
    7d34:	c6 02 00             	movb   $0x0,(%edx)
          *vidmem++ = BACKGROUND;
    7d37:	83 c2 02             	add    $0x2,%edx
    7d3a:	c6 42 ff 1e          	movb   $0x1e,-0x1(%edx)
	unsigned char *vidmem = (unsigned char *)0xB8000;
	const long size = 80*25;
  vidmem += offset*2;
	long loop;

   	for (loop=offset; loop<size; loop++) 
    7d3e:	40                   	inc    %eax
    7d3f:	eb e8                	jmp    7d29 <cls+0xf>
      if (loop >= offset){
          *vidmem++ = 0;
          *vidmem++ = BACKGROUND;
      }
   	}
}
    7d41:	5d                   	pop    %ebp
    7d42:	c3                   	ret    

00007d43 <print>:

void print(const char *_message)
{
    7d43:	55                   	push   %ebp
    7d44:	89 e5                	mov    %esp,%ebp
  unsigned char *vidmem = (unsigned char *)0xB8000;
  *vidmem++ = 'D';
    7d46:	c6 05 00 80 0b 00 44 	movb   $0x44,0xb8000
  *vidmem++ = 0x1E;
    7d4d:	c6 05 01 80 0b 00 1e 	movb   $0x1e,0xb8001
	Implemente esta funci�n, de manera tal que pueda imprimir
	el contenido de la cadena de caracteres apuntada por 
	el puntero _message.
	
*/
}
    7d54:	5d                   	pop    %ebp
    7d55:	c3                   	ret    

00007d56 <bootmain>:

int bootmain()
{
    7d56:	55                   	push   %ebp
    7d57:	89 e5                	mov    %esp,%ebp
  cls(14);
    7d59:	6a 0e                	push   $0xe
    7d5b:	e8 ba ff ff ff       	call   7d1a <cls>
  // print(cadena);
  return 0;
}
    7d60:	31 c0                	xor    %eax,%eax
    7d62:	c9                   	leave  
    7d63:	c3                   	ret    

4)	En el archivo main.c, ¿Qué hace el procedimiento cls()?
	R// EL procedimiento cls realiza un recorrido por el buffer de memoria colocando en 0 (null)
	el byte correspondiente para el caracter y colocando el valor de color de fondo.

5)	¿Comprende cómo se calculó el tamaño de la GDT?

gdt_end: 

gdtdesc:	.word	gdt_end - gdt - 1	# sizeof(gdt) - 1 
			.long	gdt			# address gdt 


	R// Dado que las etiquetas al ser ensambladas tienen una posicion de memoria podemos usarlas
	y el resultado de restar el gdt_end  con gdt y a eso restandole 1 nos dara el tamaño que tiene la tabla gdt

6)	¿Qué combinación de colores se obtiene con la configuración de ejemplo de la tabla 1?
	Se pueden obtener 15 combinaciones distintas de colores para el caso del fondo y para el caso del texto se 
	pueden hasta 8 colores distintos, segun el numero que se coloque en las posiciones se obtendra lo descrito en la siguiente
	tabla.
	Número	Color	Característica
Normal
	0	Negro		Ausencia de colores
	1	Azul		Color primario
	2	Verde		Color primario
	3	Cián		Azul + Verde
	4	Rojo		Color primario
	5	Magenta		Rojo + Azul
	6	Marrón		Rojo + Verde
	7	Blanco		Rojo + Verde + Azul
Claro
	8	Gris		Negro claro
	9	Azul claro	 
	10	Verde claro	 
	11	Cián claro	 
	12	Rojo claro	 
	13	Rosa		Magenta claro
	14	Amarillo	Marrón claro
	15	Blanco brillante	 

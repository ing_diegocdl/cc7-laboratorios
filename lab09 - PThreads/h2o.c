#include <stdio.h>
#include <pthread.h>
/***
*****************************************************************
*** Por asistencia viernes 22 =)
*****************************************************************
*/
int atomos_oxigeno = 0;
int atomos_hidrogeno = 0;

pthread_mutex_t condition_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_cond = PTHREAD_COND_INITIALIZER;

void *oxigeno() {
	
	int id = atomos_oxigeno++;
	printf("%d oxigeno\n", id);
}

void *hidrogeno() {
	int id = atomos_hidrogeno++;
	printf("%d hidrogeno\n", id);
}

int main() {
	int num;
	printf("Ingrese cantidad de moleculas H2O: ");
	scanf("%d", &num);
	
	pthread_t threads_oxigeno[num];
	pthread_t threads_hidrogeno[2*num];

	// crear atomos de hidrogeno
	int i;
	for (i = 0; i < 2*num; i++ ) {
		pthread_create(&threads_hidrogeno[i], NULL, &hidrogeno, NULL);
	}
	// crear atomos de oxigeno
	for (i = 0; i < num; i++ ) {
		pthread_create(&threads_oxigeno[i], NULL, &oxigeno, NULL);
	}

	// joins
	for (i = 0; i < 2*num; i++ ) {
		pthread_join( threads_hidrogeno[i], NULL);
	}
	for (i = 0; i < num; i++ ) {
		pthread_join(threads_oxigeno[i], NULL);
	}
	return 0;
}

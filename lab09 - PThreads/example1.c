#include <stdio.h>
#include <pthread.h>
void *hello (void * arg) {
	printf("Hello Thread\n");
}
int main() {
	pthread_t tid;
	printf("Start\n");
	pthread_create(&tid, NULL, hello, NULL);
	return 0;
}

// #include <stdio.h>
// #include <pthread.h>
// void *hello (void* arg) {
// 	printf("Hello\n");
// }
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Runner {
    public static boolean DEBUG = false;
    public static void main(String args[]) {
        if(args.length > 0 && args[0].equals("-d"))
            DEBUG = true;
        // creamos una instancia del puente :)
        Puente puente = new Puente();

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int cantidad;
        while(true){
            System.out.print("Ingrese la cantidad de personas que cruzaran el puente: ");
            try {
                cantidad = Integer.parseInt(br.readLine());
            } catch(Exception e) {
                System.out.println("Numero ingresado incorrecto");
                continue;
            }
            break;
        }

        Persona p = new Persona(puente);
        Thread threads[] = new Thread[cantidad];
        for (int i = 0; i < cantidad; i++ ) {
            threads[i] = new Thread(p);
            threads[i].setName("Persona " + i);
            threads[i].start();
        }

    }
}

public class Persona implements Runnable {
    protected Puente puente;

    public Persona(Puente p) {
        puente = p;
    }

    @Override
    public void run() {
        // se calcula aleatoreamente la direccion 0 o 1
        int direccion = (int)(Math.random()*2);
        // int direccion = Puente.IZQUIERDA;
        String strDireccion = (direccion == Puente.DERECHA)?"derecha":"izquierda";
        System.out.println(Thread.currentThread().getName() + " creada direccion: " + strDireccion);
        puente.accederAlPuente(direccion);

        // se espera entre 0 y 3 segundos
        int tiempoEspera = (int)(Math.random()*4);
        try {
            Thread.sleep(tiempoEspera);
        } catch(Exception e) {/* ignoramos */}

        puente.salirDelPuente();
    }
}

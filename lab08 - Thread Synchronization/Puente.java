public class Puente {
    public static final int VACIO = -1;
    public static final int DERECHA = 0;
    public static final int IZQUIERDA = 1;
    public static final int CAPACIDAD = 4;
    protected volatile int direccionActual = VACIO;
    protected volatile int cantidadPersonas = 0;

    public synchronized void accederAlPuente(int direccion) {
        // synchronized(this) {
            String strDireccion = (direccion == Puente.DERECHA)?"derecha":"izquierda";

            while ((direccionActual != VACIO && direccionActual != direccion) || cantidadPersonas == CAPACIDAD || !habilitado) {
                try {
                    if(Runner.DEBUG) {
                        System.out.println(Thread.currentThread().getName() + " con direccion hacia la " + strDireccion + " esperando");
                        System.out.println("\t direccion actual del puente: " + ((direccionActual == Puente.DERECHA)?"derecha":"izquierda") + ", cantidad de personas: " + cantidadPersonas);
                    }
                    wait();
                }catch(Exception e) {}
            }
            if(cantidadPersonas == 0)
                direccionActual = direccion;
            cantidadPersonas++;
            // if(cantidadPersonas == 1) {
                direccionActual = direccion;
            // }
        // }

        String threadName = Thread.currentThread().getName();
        System.out.println(threadName + " camina hacia " + strDireccion);
    }

    public synchronized void salirDelPuente() {
        // synchronized(this){
        cantidadPersonas--;
        if(cantidadPersonas == 0){
            direccionActual = VACIO;
        }
        // }
        if(Runner.DEBUG) {
            String threadName = Thread.currentThread().getName();
            System.out.println(threadName + " saliendo del puente quedan "+ cantidadPersonas + " en el puente");
        }
        // try {
        notifyAll();
        // } catch(Exception e) {}
    }
}

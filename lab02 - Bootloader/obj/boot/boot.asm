
obj/boot/boot.out:     file format elf32-i386


Disassembly of section .text:

00007c00 <start>:
.globl start
start:
  # FIRST PHASE: Register y operation mode setup.
  # Assemble for 16-bit mode
  .code16  
        cli
    7c00:	fa                   	cli    
	# The BIOS loads this code from the first sector of the hard disk into
	# memory at physical address 0x7c00 and starts executing in real mode
	# with %cs=0 %ip=7c00.
        movw     $0x07C0,%ax 
    7c01:	b8 c0 07 8e d8       	mov    $0xd88e07c0,%eax
        movw     %ax, %ds
        movw     %ax, %es
    7c06:	8e c0                	mov    %eax,%es
        movw     %ax, %fs
    7c08:	8e e0                	mov    %eax,%fs
        movw     %ax, %gs
    7c0a:	8e e8                	mov    %eax,%gs

	#Stack Initialization
        movw     $0x0000 , %ax 
    7c0c:	b8 00 00 8e d0       	mov    $0xd08e0000,%eax
        movw     %ax, %ss
        movw     $0xFFFF,%sp
    7c11:	bc ff ff fb b8       	mov    $0xb8fbffff,%esp
        sti
   

	movw     $0x0043, %ax 	#Put an ASCII Value to ax
    7c16:	43                   	inc    %ebx
    7c17:	00 b4 0e b7 00 b3 07 	add    %dh,0x7b300b7(%esi,%ecx,1)
        movb     $0x0E,%ah 	#Bios Teletype,This number must be used to 
				#tell the BIOS to put a character on the screen
        movb     $0x00,%bh 	#Page number (For most of our work this will remain 0x00)
        movb     $0x07,%bl      #Text attribute (For most of our work this will remain 0x07) 
				#Try to change this value
        int      $0x10  	#BIOS video interrupt. 
    7c1e:	cd 10                	int    $0x10

	push     $0x0045
    7c20:	6a 45                	push   $0x45
	call	 print
    7c22:	e8 1f 00 6a 46       	call   466a7c46 <_end+0x4669ff8e>

	push     $0x0046 	#Put an ASCII Value to ax
	call	 print
    7c27:	e8 1a 00 6a 47       	call   476a7c46 <_end+0x4769ff8e>
	push	 $0x0047
	call	 print
    7c2c:	e8 15 00 6a 48       	call   486a7c46 <_end+0x4869ff8e>
	push	 $0x0048
	call	 print
    7c31:	e8 10 00 31 f6       	call   f6317c46 <_end+0xf630ff8e>

	xor 	%si, %si
	movw	 $str, %si
    7c36:	be ac 7c e8 23       	mov    $0x23e87cac,%esi
	call Println
    7c3b:	00 6a 49             	add    %ch,0x49(%edx)


	push	 $0x0049
	call	 print
    7c3e:	e8 03 00 e8 2c       	call   2ce87c46 <_end+0x2ce7ff8e>
	...

00007c44 <print>:
.globl print
.text
print:
   .code16
	# save registers
	pop %bx
    7c44:	5b                   	pop    %ebx
	pop %ax
    7c45:	58                   	pop    %eax
	push %bx
    7c46:	53                   	push   %ebx

        movb     $0x0E,%ah 	#Bios Teletype,This number must be used to 
    7c47:	b4 0e                	mov    $0xe,%ah
				#tell the BIOS to put a character on the screen
        movb     $0x00,%bh 	#Page number (For most of our work this will remain 0x00)
    7c49:	b7 00                	mov    $0x0,%bh
        movb     $0x07,%bl      #Text attribute (For most of our work this will remain 0x07) 
    7c4b:	b3 07                	mov    $0x7,%bl
				#Try to change this value
        int      $0x10  	#BIOS video interrupt. 
    7c4d:	cd 10                	int    $0x10


	ret
    7c4f:	c3                   	ret    

00007c50 <PrintNwL>:
     
#Prints empty new lines like '\n' in C/C++ 	
PrintNwL: 
    mov 0, %al	# null terminator '\0'
    7c50:	a0 00 00 aa b4       	mov    0xb4aa0000,%al
    stosb       # Store string 

    # Adds a newline break '\n'
    mov $0x0E, %ah
    7c55:	0e                   	push   %cs
    mov $0x0D, %al
    7c56:	b0 0d                	mov    $0xd,%al
    int $0x10
    7c58:	cd 10                	int    $0x10
    mov $0x0A, %al
    7c5a:	b0 0a                	mov    $0xa,%al
    int $0x10
    7c5c:	cd 10                	int    $0x10
	ret
    7c5e:	c3                   	ret    

00007c5f <Println>:

Println:
    lodsb # Load string 
    7c5f:	ac                   	lods   %ds:(%esi),%al
    or %al, %al
    7c60:	08 c0                	or     %al,%al
    jz complete
    7c62:	74 06                	je     7c6a <complete>
    mov $0x0e, %ah
    7c64:	b4 0e                	mov    $0xe,%ah
    int $0x10 # BIOS Interrupt 0x10 - Used to print characters on the screen via Video Memory 
    7c66:	cd 10                	int    $0x10
    jmp Println # Loop   	
    7c68:	eb f5                	jmp    7c5f <Println>

00007c6a <complete>:
complete:
    call PrintNwL
    7c6a:	e8 e3 ff c3 66       	call   66c47c52 <_end+0x66c3ff9a>
    ret
    7c6f:	90                   	nop

00007c70 <bootmain>:
 **********************************************************************/

extern void print(char x);
void
bootmain(void)
{
    7c70:	55                   	push   %ebp
    7c71:	89 e5                	mov    %esp,%ebp
    7c73:	eb fe                	jmp    7c73 <bootmain+0x3>
